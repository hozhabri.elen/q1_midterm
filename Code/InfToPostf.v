//use internet for this algorithm and know how to use function in verilog

module InfToPostf #(parameter N) (
    input [N*N-1:0] infix_expr,
    output reg [N*N-1:0] postfix_expr
);

    function integer precedence(input [N-1:0] op);
        begin
            case (op)
                "+": precedence = 1;
                "*": precedence = 2;
                default: precedence = 0;
            endcase
        end
    endfunction

    integer i, j, exp_sp;
    reg [N-1:0] token;
    reg [N-1:0] exp_stack [2*N-1:0];

    always @(*) begin
        j = 0;
        exp_sp = 0;
        for (i = 0; i < N*N-1; i = i + 1) begin
            token = infix_expr[i*N +: N];
            if (token >= "0" && token <= "9") begin
                postfix_expr[j*N +: N] = token;
                j = j + 1;
            end else if (token == "(") begin
                exp_stack[exp_sp] = token;
                exp_sp = exp_sp + 1;
            end else if (token == ")") begin
                while (exp_sp > 0 && exp_stack[exp_sp-1] != "(") begin
                    postfix_expr[j*N +: N] = exp_stack[exp_sp-1];
                    j = j + 1;
                    exp_sp = exp_sp - 1;
                end
                if (exp_sp > 0) 
			exp_sp = exp_sp - 1;
            end else if (token == "+" || token == "*") begin
                while (exp_sp > 0 && precedence(exp_stack[exp_sp-1]) >= precedence(token)) begin
                    postfix_expr[j*N +: N] = exp_stack[exp_sp-1];
                    j = j + 1;
                    exp_sp = exp_sp - 1;
                end
                exp_stack[exp_sp] = token;
                exp_sp = exp_sp + 1;
            end
        end
        while (exp_sp > 0) begin
            postfix_expr[j*N +: N] = exp_stack[exp_sp-1];
            j = j + 1;
            exp_sp = exp_sp - 1;
        end
	for (; j < N*N-1; j = j + 1) begin
            postfix_expr[j*N +: N] = 8'b0;
        end
    end
endmodule
