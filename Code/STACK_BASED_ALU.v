module STACK_BASED_ALU #(parameter N) (
    input [2:0] opcode,
    input signed [N-1:0] input_data,
    output reg signed [N-1:0] output_data,
    output reg overflow
);
    reg signed [N-1:0] stack [0:2*N-1];
    reg [N/2:0] sp; // stack_pointer

    initial begin
        sp = 0;
    end

    always @(*) begin
        overflow = 0;
        case (opcode)
            3'b100: begin // Addition
                output_data = stack[sp-1] + stack[sp-2];
                overflow = (output_data[N-1] != stack[sp-1][N-1]) && (stack[sp-1][N-1] == stack[sp-2][N-1]);
            end
            3'b101: begin // Multiply
                output_data = stack[sp-1] * stack[sp-2];
                overflow = (output_data != (stack[sp-1] * stack[sp-2]));
            end
            3'b110: begin // PUSH
                stack[sp] = input_data;
                output_data = stack[sp];
                sp = sp + 1;
            end
            3'b111: begin // POP
		if(sp>0) begin
               	   sp = sp - 1;
                   output_data = stack[sp];
		end
		else begin
			output_data = 0;
		end
            end
            default: begin // No Operation
                output_data = output_data;
            end
        endcase
    end
endmodule
