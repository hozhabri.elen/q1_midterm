module TB_AnotherModule;
    parameter N = 8;
    reg [N*N-1:0] postfix_expr;
    wire signed [N-1:0] result;
    wire overflow;

    postfix_calculator #(N) calculate (postfix_expr,result,overflow);

    initial begin
        postfix_expr = "2*3+(10+4+3)*(-20)+(6+5)";
        #10;

        $display("Result: %d", result);
        $display("Overflow: %b", overflow);

        #10 $stop;
    end
endmodule
