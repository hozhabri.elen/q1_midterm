module expression_evaluator #(parameter N) (
    input [N*N-1:0] infix_expr, 
    output reg signed [N-1:0] result,
    output reg overflow
);

    reg signed [N-1:0] input_data;
    reg [2:0] opcode;
    wire signed [N-1:0] output_data;
    wire alu_overflow;
    reg signed [N-1:0] stack [2*N-1:0];
    reg [3:0] sp;
    reg [N*N-1:0] postfix_expr;

    STACK_BASED_ALU #(N) alu (opcode,input_data,output_data,alu_overflow);
    InfToPostf #(N) inf (infix_expr,postfix_expr);
    evaluate_postfix(postfix_expr);

    task evaluate_postfix(input [N*N-1:0] postfix);
        integer i;
        reg [N-1:0] token;
        begin
            sp = 0;
            overflow = 0;

            for (i = 0; i < N*N-1; i = i + 1) begin
                token = postfix[i*N +: N];
                if (token >= "0" && token <= "9") begin

                    input_data = token - "0";
                    opcode = 3'b110; // PUSH
                    #1;
                end else if (token == "+") begin
                    opcode = 3'b100; // ADD
                    #1;
                    sp = sp - 1;
                    stack[sp-1] = output_data;
                    overflow = overflow || alu_overflow;
                end else if (token == "*") begin
                    opcode = 3'b101; // MULTIPLY
                    #1;
                    sp = sp - 1;
                    stack[sp-1] = output_data;
                    overflow = overflow || alu_overflow;
                end
            end

            result = stack[0];
        end
    endtask

endmodule
