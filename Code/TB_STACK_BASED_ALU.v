module TB_STACK_BASED_ALU;
    reg signed [3:0] input_data4;
    reg signed [7:0] input_data8;
    reg signed [15:0] input_data16;
    reg signed [31:0] input_data32;
    reg [2:0] opcode;
    wire signed [3:0] output_data4;
    wire signed [7:0] output_data8;
    wire signed [15:0] output_data16;
    wire signed [31:0] output_data32;
    wire overflow4;
    wire overflow8;
    wire overflow16;
    wire overflow32;

    STACK_BASED_ALU #(4) alu4 (opcode,input_data4,output_data4,overflow4);
    STACK_BASED_ALU #(8) alu8 (opcode,input_data8,output_data8,overflow8);
    STACK_BASED_ALU #(16) alu16 (opcode,input_data16,output_data16,overflow16);
    STACK_BASED_ALU #(32) alu32 (opcode,input_data32,output_data32,overflow32);

    initial begin
        input_data4 = 0;
        input_data8 = 0;
        input_data16 = 0;
        input_data32 = 0;
        opcode = 3'b000;
	#10
	$display("Time: %t, opcode=%b",$time, opcode);
        $display("For 4-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data4, output_data4, overflow4);
        $display("For 8-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data8, output_data8, overflow8);
        $display("For 16-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data16, output_data16, overflow16);
        $display("For 32-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data32, output_data32, overflow32);

        #10
        input_data4 = 8;     //PUSH
        input_data8 = 8;     //PUSH
        input_data16 = 8;     //PUSH
        input_data32 = 8;     //PUSH
        opcode = 3'b110;
	#10
	$display("Time: %t, opcode=%b",$time, opcode);
        $display("For 4-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data4, output_data4, overflow4);
        $display("For 8-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data8, output_data8, overflow8);
        $display("For 16-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data16, output_data16, overflow16);
        $display("For 32-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data32, output_data32, overflow32);

        #10;
        input_data4 = 3;    //PUSH
        input_data8 = 3;    //PUSH
        input_data16 = 3;    //PUSH
        input_data32 = 3;    //PUSH
        opcode = 3'b110;
	#10
	$display("Time: %t, opcode=%b",$time, opcode);
        $display("For 4-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data4, output_data4, overflow4);
        $display("For 8-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data8, output_data8, overflow8);
        $display("For 16-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data16, output_data16, overflow16);
        $display("For 32-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data32, output_data32, overflow32);

        #10;
        opcode = 3'b100;   //Addition
	#10
	$display("Time: %t, opcode=%b",$time, opcode);
        $display("For 4-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data4, output_data4, overflow4);
        $display("For 8-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data8, output_data8, overflow8);
        $display("For 16-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data16, output_data16, overflow16);
        $display("For 32-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data32, output_data32, overflow32);


	// Multiply
	#10;
        opcode = 3'b101;
	#10;
	$display("Time: %t, opcode=%b",$time, opcode);
        $display("For 4-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data4, output_data4, overflow4);
        $display("For 8-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data8, output_data8, overflow8);
        $display("For 16-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data16, output_data16, overflow16);
        $display("For 32-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data32, output_data32, overflow32);

        #10;
        opcode = 3'b111;   //POP
	#10
	$display("Time: %t, opcode=%b",$time, opcode);
        $display("For 4-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data4, output_data4, overflow4);
        $display("For 8-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data8, output_data8, overflow8);
        $display("For 16-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data16, output_data16, overflow16);
        $display("For 32-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data32, output_data32, overflow32);
        
        // Check for overflow
	#10;
        input_data4 = 8;
        input_data8 = 127;
        opcode = 3'b110;
	#10
	$display("Time: %t, opcode=%b",$time, opcode);
        $display("For 4-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data4, output_data4, overflow4);
        $display("For 8-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data8, output_data8, overflow8);

        #10;
        input_data4 = 8;
        input_data8 = 127;
        opcode = 3'b110;
	#10
	$display("Time: %t, opcode=%b",$time, opcode);
        $display("For 4-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data4, output_data4, overflow4);
        $display("For 8-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data8, output_data8, overflow8);

        #10;
        opcode = 3'b100;
	#10
	$display("Time: %t, opcode=%b",$time, opcode);
        $display("For 4-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data4, output_data4, overflow4);
        $display("For 8-bit: input_data=%d, output_data=%d, overflow=%b",
                 input_data8, output_data8, overflow8);
        #10;
        $stop;
    end

endmodule
